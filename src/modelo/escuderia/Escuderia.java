package modelo.escuderia;

import modelo.circuito.Circuito;
import modelo.piloto.Piloto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Escuderia implements Serializable {
    private int id;
    private String nombre;
    private float presupeuesto;
    private LocalDate funcdacion;
    private Circuito corcuito;

    public Escuderia() {
    }

    public Escuderia(int id, String nombre, float presupeuesto, LocalDate funcdacion, ArrayList<Piloto> pilotos, Circuito corcuito) {
        this.id = id;
        this.nombre = nombre;
        this.presupeuesto = presupeuesto;
        this.funcdacion = funcdacion;
        this.corcuito = corcuito;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPresupeuesto() {
        return presupeuesto;
    }

    public void setPresupeuesto(float presupeuesto) {
        this.presupeuesto = presupeuesto;
    }

    public LocalDate getFuncdacion() {
        return funcdacion;
    }

    public void setFuncdacion(LocalDate funcdacion) {
        this.funcdacion = funcdacion;
    }


    public Circuito getCorcuito() {
        return corcuito;
    }

    public void setCorcuito(Circuito corcuito) {
        this.corcuito = corcuito;
    }

    @Override
    public String toString() {
        return  String.format("%010d - %s", id , nombre );
    }
}
