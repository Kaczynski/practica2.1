package modelo.escuderia;

import modelo.circuito.Circuito;
import modelo.piloto.Piloto;

import java.time.LocalDate;
import java.util.ArrayList;

public class ModeloEscuderia {
    private ArrayList<Escuderia> escuderias;

    public ModeloEscuderia(ArrayList<Escuderia> escuderias) {
        this.escuderias = escuderias;
    }

    public ModeloEscuderia() {
        this.escuderias = new ArrayList<>();
    }

    /**
     * Anade el Escuderia a la lista.
     *
     * @see Escuderia
     */
    public void add(int id, String nombre, float presupeuesto, LocalDate funcdacion, ArrayList<Piloto> pilotos, Circuito corcuito) {
        escuderias.add(new Escuderia(id, nombre, presupeuesto, funcdacion, pilotos,corcuito));
    }

    /**
     * Actualiza la informacion de la Escuderia
     * @param id int
     * @param nombre String
     * @param presupeuesto float
     * @param funcdacion LocalDate
     * @param corcuito Circuito
     */
    public void update(int id, String nombre, float presupeuesto, LocalDate funcdacion,  Circuito corcuito) {
        Escuderia e = this.findById(id);
        e.setNombre(nombre);
        e.setPresupeuesto(presupeuesto);
        e.setFuncdacion(funcdacion);
        e.setCorcuito(corcuito);
    }

    /**
     * Busca en Escuderias por el id
     *
     * @param id int Id a buscar
     * @return Escuderia que coincide con el id o null si no hay coincidencias.
     */
    public Escuderia findById(int id) {
        for (Escuderia c : this.escuderias) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    /**
     * Busca en la lista de Escuderias el nombre
     *
     * @param name String Nombre a buscar
     * @return Escuderia que coincide con el nombre o null si no hay coincidencias.
     * @see Escuderia
     */
    public Escuderia findByName(String name) {
        for (Escuderia c : this.escuderias) {
            if (c.getNombre().equals(name)) {
                return c;
            }
        }
        return null;
    }

    /**
     * Elimina la escuderia con el id especificado
     * @param id int
     */
    public void remove(int id){
        this.escuderias.remove(this.findById(id));
    }

    /**
     * Busca la escuderia del Circuito
     * @return Escuderia o null si no pertenece a ninguna
     */
    public Escuderia findCircuito(int id){
        for (Escuderia e : this.escuderias){
            if(e.getCorcuito().getId() == id){
                return e;
            }
        }
        return null;
    }

    public ArrayList<Escuderia> search(String search){
        ArrayList<Escuderia> escuderias = new ArrayList<>();
        for (Escuderia escuderia: this.escuderias){
            if(escuderia.getNombre().contains(search)){
                escuderias.add(escuderia);
            }
        }

            for (Escuderia escuderia: this.escuderias){
                if(Integer.toString(escuderia.getId()).contains(search)){
                    escuderias.add(escuderia);
                }
            }

        return escuderias;
    }

    public ArrayList<Escuderia> getEscuderias() {
        return this.escuderias;
    }

    public void setEscuderias(ArrayList<Escuderia> escuderias) {
        this.escuderias = escuderias;
    }
}
