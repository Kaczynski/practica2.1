package modelo.circuito;

import java.io.Serializable;
import java.time.LocalDate;



public class Circuito implements Serializable {
    private int id;
    private String nombre;
    private String localidad;
    private float longitud;
    private LocalDate carrera;

    /**
     *
     * @param id Identificador unico
     * @param nombre Nombre
     * @param localidad Localidad
     * @param longitud Longitud en metros
     * @param carrera Fecha de la primera carrera
     */
    public Circuito(int id, String nombre, String localidad, float longitud, LocalDate carrera) {
        this.id = id;
        this.nombre = nombre;
        this.localidad = localidad;
        this.longitud = longitud;
        this.carrera = carrera;
    }

    public Circuito() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

    public LocalDate getCarrera() {
        return carrera;
    }

    public void setCarrera(LocalDate carrera) {
        this.carrera = carrera;
    }

    @Override
    public String toString() {
        return nombre+ ", "+localidad;
    }
}
