package modelo.circuito;

import java.time.LocalDate;
import java.util.ArrayList;

public class ModeloCircuito {
    private ArrayList<Circuito> circuitos;

    public ModeloCircuito(ArrayList<Circuito> circuitos) {
        this.circuitos = circuitos;
    }

    public ModeloCircuito() {
        this.circuitos = new ArrayList<>();
    }

    /**
     * Anade el Circuito a la lista.
     *
     * @see Circuito
     */
    public void add(int id, String nombre, String localidad, float longitud, LocalDate carrera) {
        circuitos.add(new Circuito(id, nombre, localidad, longitud, carrera));
    }

    /**
     * Actualiza el Circuito
     * @param id int
     * @param nombre String
     * @param localidad String
     * @param longitud float
     * @param carrera LocalDate
     */
    public void update(int id, String nombre, String localidad, float longitud, LocalDate carrera) {
        Circuito c = this.findById(id);
        c.setNombre(nombre);
        c.setLocalidad(localidad);
        c.setLongitud(longitud);
        c.setCarrera(carrera);
    }

    /**
     * Busca en circuitos por el id
     *
     * @param id int Id a buscar
     * @return Circuito que coincide con el id o null si no hay coincidencias.
     */
    public Circuito findById(int id) {
        for (Circuito c : this.circuitos) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    /**
     * Busca en la lista de circuitos el nombre
     *
     * @param name String Nombre a buscar
     * @return Circuito que coincide con el nombre o null si no hay coincidencias.
     * @see Circuito
     */
    public Circuito findByName(String name) {
        for (Circuito c : this.circuitos) {
            if (c.getNombre().equals(name)) {
                return c;
            }
        }
        return null;
    }

    /**
     * Elimina el corcito con el id espoecificado
     * @param id int
     */
    public void remove(int id){
        this.circuitos.remove(this.findById(id));
    }


    /**
     * Busca Circuitos done la condicion coincida con el id, nombre o localidad
     * @param search String
     * @return ArrayList de Circuito
     */
    public ArrayList<Circuito> search(String search){
        ArrayList<Circuito> circuitos = new ArrayList<>();
        for (Circuito escuderia: this.circuitos){
            if(escuderia.getNombre().contains(search) || escuderia.getLocalidad().contains(search)
                    || Integer.toString(escuderia.getId()).contains(search) ){
                circuitos.add(escuderia);
            }
        }


        return circuitos;
    }

    public ArrayList<Circuito> getCircuitos() {
        return circuitos;
    }

    public void setCircuitos(ArrayList<Circuito> circuitos) {
        this.circuitos = circuitos;
    }
}
