package modelo.piloto;

import Utils.LocalDateAdapter;
import modelo.escuderia.Escuderia;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;

public class Piloto implements Serializable {
    private int id;
    private String nombre;
    private String apellido;
    private float valoracion;
    private Escuderia escuderia;
    private LocalDate edad;

    public Piloto(int id, String nombre, String apellido, float valoracion, LocalDate edad, Escuderia escuderia) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.valoracion = valoracion;
        this.edad = edad;
        this.escuderia = escuderia;
    }

    public Piloto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public LocalDate getEdad() {
        return edad;
    }

    public void setEdad(LocalDate edad) {
        this.edad = edad;
    }

    public Escuderia getEscuderia() {
        return escuderia;
    }

    public void setEscuderia(Escuderia escuderia) {
        this.escuderia = escuderia;
    }

    @Override
    public String toString() {
        return String.format("%-20s",(nombre +", "+apellido));
    }
}
