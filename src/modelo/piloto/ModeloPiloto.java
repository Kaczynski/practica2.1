package modelo.piloto;

import modelo.escuderia.Escuderia;

import java.time.LocalDate;
import java.util.ArrayList;

public class ModeloPiloto {
    private ArrayList<Piloto> pilotos;

    public ModeloPiloto(ArrayList<Piloto> pilotos) {
        this.pilotos = pilotos;
    }

    public ModeloPiloto() {
        this.pilotos = new ArrayList<>();
    }

    /**
     * Anade el Piloto a la lista.
     *
     * @see Piloto
     */
    public void add(int id, String nombre, String apellido, float valoracion, LocalDate edad, Escuderia escuderia) {
        pilotos.add(new Piloto(id, nombre, apellido, valoracion, edad,escuderia));
    }

    public void update(int id, String nombre, String apellido, float valoracion, LocalDate edad,Escuderia escuderia) {
        Piloto p = this.findById(id);
        p.setNombre(nombre);
        p.setApellido(apellido);
        p.setValoracion(valoracion);
        p.setEdad(edad);
        p.setEscuderia(escuderia);
    }

    /**
     * Busca en pilotos por el id
     *
     * @param id int Id a buscar
     * @return Piloto que coincide con el id o null si no hay coincidencias.
     */
    public Piloto findById(int id) {
        for (Piloto c : this.pilotos) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    /**
     * Busca en la lista de pilotos el nombre
     *
     * @param name String Nombre a buscar
     * @return Piloto que coincide con el nombre o null si no hay coincidencias.
     * @see Piloto
     */
    public Piloto findByName(String name) {
        for (Piloto c : this.pilotos) {
            if (c.getNombre().equals(name)) {
                return c;
            }
        }
        return null;
    }

    /**
     * Elimina el piloto con el id especificado
     *
     * @param id int
     */
    public void remove(int id) {
        this.pilotos.remove(findById(id));
    }

    /**
     * Busca Circuitos done la condicion coincida con el id, nombre o localidad
     * @param search String
     * @return ArrayList de Circuito
     */
    public ArrayList<Piloto> search(String search){
        ArrayList<Piloto> circuitos = new ArrayList<>();
        for (Piloto escuderia: this.pilotos){
            if(escuderia.getNombre().contains(search) || escuderia.getApellido().contains(search)
                    || Integer.toString(escuderia.getId()).contains(search) ){
                circuitos.add(escuderia);
            }
        }



        return circuitos;
    }


    public ArrayList<Piloto> getPilotos() {
        return pilotos;
    }

    public void setPilotos(ArrayList<Piloto> pilotos) {
        this.pilotos = pilotos;
    }
}
